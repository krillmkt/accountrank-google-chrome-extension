

chrome.tabs.executeScript(null, {
    file: "inject.js"
}, function( content ) {
    const textarea = document.getElementById("target");
    const btn      = document.getElementById("btn");

    btn.addEventListener("click", function () {
        textarea.select();
        document.execCommand("copy");
        btn.innerHTML = "Text Copied to clipboard!"
    });
    textarea.value = content;
});
