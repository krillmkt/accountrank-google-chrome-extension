(function() {
    function extractScores () {
        let scores = document.querySelectorAll(".thresh-tick-score__score");
        scores.forEach( score => {
            data.rankbar.scores.push(parseFloat(score.innerHTML))
        });
    };
    
    function extractSlits () {
        let slits          = document.querySelectorAll(".slit"),
        extractOpacity = ( rgba ) => {
            let rgbaArr = rgba.match(/\(([^)]+)\)/)[1].replace(/\s/g,'').split(",");
            return parseFloat( rgbaArr[rgbaArr.length-1] );
        },
        extractPosition = ( left ) => {
            return `${(parseFloat( left ) * 100 / 1000)}%`;
        };
    
        slits.forEach(slit => {
    
            let { backgroundColor, left } = slit.style
                entry = {
                    position: 0,
                    opacity: 1
                }
    
            if ( backgroundColor.indexOf("rgba") != -1 ) {
                entry.opacity = extractOpacity(backgroundColor);
            }
    
            entry.position = extractPosition(left);
    
            data.rankbar.series.push(entry);
        });
    };
    
    function extractEntities () {
        let currentBar = document.querySelector(".small-vert-bar-chart__bar-container:nth-child(3)"),
            totalWon = parseFloat(currentBar.querySelector(".small-vert-bar-chart__bar-heading").textContent);
    
        ["A", "B", "C", "D"].forEach( grade => {
            
            document.querySelector(`.small-vert-bar-chart__${grade}`).dispatchEvent( new Event("mouseover") );
    
                let entity = {
                    grade: grade,
                    amount: parseFloat(document.querySelector(".stats-section__desc__note span:nth-child(2)").textContent.replace(/[^0-9.-]+/g,""))
                    ,
                    converted: parseFloat(document.querySelector(".stats-section__desc__note span:nth-child(3)").textContent.replace(/[^0-9.-]+/g,""))
                    ,
                    won: Math.round(totalWon * parseFloat(currentBar.querySelector(`.small-vert-bar-chart__${grade}`).style.height)/100),
                    wonRevenue: parseFloat(document.querySelector(".stats-section__desc__note span:nth-child(4) span").textContent.replace(/[^0-9.-]+/g,""))
                }
    
                data.entities.push(entity)
    
    
        });
    };

    let data = {
        global: {
            status: document.querySelector(".behavior-overview__modelstatus .behavior-overview__modelstatus__row:nth-child(1) .behavior-overview__modelstatus__value").innerHTML,
            model: document.querySelector(".behavior-overview__modelstatus .behavior-overview__modelstatus__row:nth-child(2) .behavior-overview__modelstatus__value").innerHTML,
            entity: document.querySelector(".behavior-overview__modelstatus .behavior-overview__modelstatus__row:nth-child(3) .behavior-overview__modelstatus__value").innerHTML,
            signals: document.querySelector(".behavior-overview__modelstatus .behavior-overview__modelstatus__row:nth-child(4) .behavior-overview__modelstatus__value").innerHTML,
            description: document.querySelector(".behavior-overview__modeldescription div").innerHTML.replace("Infer", "AccountRank™"),
            updated: document.querySelector(".behavior-overview__modelstatus .behavior-overview__modelstatus__row:nth-child(5) .behavior-overview__modelstatus__value").innerHTML
        },
        entities: [],
        rankbar: {
            scores: [],
            series: []
        }
    }

    extractEntities();
    extractScores();
    extractSlits();
    
    return JSON.stringify(data)
})()